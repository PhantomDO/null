using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exemple1 : MonoBehaviour
{
    [SerializeField]
    private int identifiant = 3;
    private int x;

    // Start is called before the first frame update
    void Start()
    {
        //identifiant = 3;
        x = 1;
        Debug.Log($"Identifiant: {identifiant}, X: {x}");
    }
}
