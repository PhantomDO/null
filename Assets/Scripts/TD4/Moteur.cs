using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Moteur
{
    public float Essence { get; set; }

    public Moteur()
    {
        Essence = 10;
        Debug.Log($"Essence: {Essence}");
    }

    public bool Roule(float consommation)
    {
        Essence = Mathf.Max(0, Essence - consommation);
        return Essence > 0.0f;
    }
}
