using System;
using System.Collections;
using System.Collections.Generic;
using TD7;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] private bool _automaticMovement = false;
    [SerializeField] private float _moveSpeed = 2.5f;
    [SerializeField] private float _fuelLiterBurnPerFrame = 5f;
    [SerializeField] private Transform[] _wheels;
    [SerializeField] private Moteur _engine;

    private ChangeChildsRendererColor _changeChildsRendererColor;


    private void Awake()
    {
        _engine = new Moteur();
        if (TryGetComponent(out _changeChildsRendererColor))
        {
            Debug.Log($"ChangeChildRendererColor founded");
        }
        else
        {
            Debug.LogWarning($"ChangeChildRendererColor not found");
        }
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 axis = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
        if (_automaticMovement)
        {
            axis.z = 1f;
        }

        if (axis.magnitude > Mathf.Epsilon)
        {
            Move(axis);
            RotateWheels(axis);
        }
    }

    private void Move(Vector3 axis)
    {
        var position = transform.position;
        var rotation = Quaternion.LookRotation(transform.forward + (transform.right * axis.x));

        bool canMove = _engine.Roule(_fuelLiterBurnPerFrame * Time.deltaTime);
        if (canMove)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * _moveSpeed);
            transform.position = Vector3.MoveTowards(position, position + (transform.forward * axis.z), _moveSpeed * Time.fixedDeltaTime);

        }

        _changeChildsRendererColor.ChangeColor((canMove ? 1 : 0) * _moveSpeed, "wheel");
    }

    private void RotateWheels(Vector3 axis)
    {
        if (_wheels.Length <= 0) return;
        foreach (var wheel in _wheels)
        {
            if (wheel.CompareTag("Front_Wheel"))
            {
                float yAngle = 45f;
                wheel.localRotation = Quaternion.RotateTowards(wheel.localRotation, 
                    Quaternion.Euler(0, axis.x * yAngle, 0), _moveSpeed * (yAngle/10) * 60 * Time.deltaTime);
            }

            float speed = _moveSpeed;
            float movement = 60 * Time.deltaTime;
            wheel.Rotate(axis.z * speed * 360 * movement, 0, 0);

        }
    }
}
