using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Calcul2 : MonoBehaviour
{
    public float moveSpeed = 3;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var rotation = Quaternion.LookRotation(transform.forward + (transform.right / 2));
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime);
        transform.position = Vector3.MoveTowards(transform.position,
            transform.position + transform.forward, moveSpeed * Time.deltaTime);
    }
}
