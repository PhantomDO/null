using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalculVector : MonoBehaviour
{
    public Vector2[] a2D;
    public Vector2[] b2D;

    public Vector3[] a3D;
    public Vector3[] b3D;

    // Start is called before the first frame update
    void Start()
    {
        test();
    }

    private void test()
    {
        if (a2D.Length != b2D.Length)
        {
            Debug.LogError($"The two array has to have the same number of elements.");
            return;
        }

        // Add
        Debug.Log($"Addition Vec2D: {a2D[0]} + {b2D[0]} = {a2D[0] + b2D[0]}");
        // Sub
        Debug.Log($"Soustraction Vec2D: {a2D[0]} - {b2D[0]} = {a2D[0] - b2D[0]}");

        if (a3D.Length != b3D.Length)
        {
            Debug.LogError($"The two array has to have the same number of elements.");
            return;
        }

        // Add
        Debug.Log($"Addition Vec3D: {a3D[0]} + {b3D[0]} = {a3D[0] + b3D[0]}");
        // Add
        Debug.Log($"Soustraction Vec3D: {a3D[0]} - {b3D[0]} = {a3D[0] - b3D[0]}");
    }

}
