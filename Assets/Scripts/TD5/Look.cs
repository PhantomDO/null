using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Look : MonoBehaviour
{
    public float moveSpeed = 2.5f;
    public float maxDistanceToMove = 25f;
    public GameObject target;

    private Vector3 _offset;

    private void Start()
    {
        _offset = target.transform.position - transform.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (!target) return;
        transform.LookAt(target.transform);

        float distance = Vector3.Distance(target.transform.position, transform.position);
        if (distance > maxDistanceToMove + Mathf.Epsilon)
        {
            var movement = ((target.transform.position - transform.position) - _offset);
            transform.position = Vector3.MoveTowards(transform.position,
                transform.position + movement.normalized, distance * moveSpeed * Time.smoothDeltaTime);
        }
    }
}
