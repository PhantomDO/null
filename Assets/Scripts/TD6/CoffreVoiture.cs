using System;
using UnityEngine;

namespace TD6
{
    [Serializable]
    public class CoffreVoiture : IDisposable
    {
        [SerializeField]
        private int _liter;

        public CoffreVoiture()
        {
            _liter = 40;
            Debug.Log($"Le coffre de la voiture � �t� cr��.");
        }

        public CoffreVoiture(int liter)
        {
            _liter = liter;
            Debug.Log($"Le coffre de la voiture � �t� cr�� avec l'argument suivant {liter}");
        }

        ~CoffreVoiture()
        {
            ReleaseUnmanagedResources();
        }

        private void ReleaseUnmanagedResources()
        {
            Debug.Log($"Le coffre de la voiture � �t� d�truit.");
        }

        public void Dispose()
        {
            ReleaseUnmanagedResources();
            GC.SuppressFinalize(this);
        }
    }
}