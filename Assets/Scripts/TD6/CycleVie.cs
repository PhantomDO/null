using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace TD6
{
    public class CycleVie : MonoBehaviour
    {
        private CoffreVoiture _coffre;

        private void Awake()
        {
            Debug.Log($"La voiture se réveil.");
        }

        // Start is called before the first frame update
        void Start()
        {
            _coffre = new CoffreVoiture(Random.Range(30, 120));
            Debug.Log($"La voiture fini son paramétrage juste avant son utilisation.");
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Destroy(gameObject);
            }
        }

        private void OnDestroy()
        {
            _coffre.Dispose();
            Debug.Log($"La  voiture  est  en  voie  de  destruction.");
        }
    }
}
