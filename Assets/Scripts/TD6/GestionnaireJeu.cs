using System.Collections;
using System.Collections.Generic;
using TD6;
using UnityEngine;

public class GestionnaireJeu : MonoBehaviour
{
    [SerializeField]
    private List<CoffreVoiture> _coffres;


    // Start is called before the first frame update
    void Start()
    {
        int rdm = Random.Range(1, 12);
        _coffres = new List<CoffreVoiture>(12);
        for (int i = 0; i < rdm; i++)
        {
            _coffres.Add(new CoffreVoiture(Random.Range(30, 120)));
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            foreach (var v in _coffres)
            {
                v.Dispose();
            }

            _coffres.Clear();
        }
    }
}
