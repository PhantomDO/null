﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace TD7
{
    public class ChangeChildsRendererColor : MonoBehaviour
    {
        public Color color = Color.magenta;
        public float speedColorChange = 2f;
        private bool _swapColor;
        private List<Color> _baseColors;
        private List<MeshRenderer> _meshRenderers;

        private void Start()
        {
            // find renderers
            _baseColors = new List<Color>();
            _meshRenderers = new List<MeshRenderer>();
            foreach (Transform tr in transform)
            {
                if (tr.TryGetComponent(out MeshRenderer mr))
                {
                    _meshRenderers.Add(mr);
                    _baseColors.Add(mr.material.color);
                }
            }
        }

        public void ChangeColor(float value, string containStrInName)
        {
            _swapColor = value >= speedColorChange;
            for (int i = 0; i < _meshRenderers.Count; i++)
            {
                _meshRenderers[i].material.color = _meshRenderers[i].name.ToLower().Contains(containStrInName.ToLower()) && _swapColor 
                    ? color : _baseColors[i];
            }
        }
    }
}