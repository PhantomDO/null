using UnityEngine;

namespace TD7
{
    public class ChangeRendererColor : MonoBehaviour
    {
        public Color color = Color.magenta;
        private Color _baseColor;
        private bool _swapColor;
        private MeshRenderer _meshRenderer;

        // Start is called before the first frame update
        void Start()
        {
            if (TryGetComponent(out _meshRenderer))
            {
                Debug.Log($"Found meshrender");
                _baseColor = _meshRenderer.material.color;
            }
            else
            {
                Debug.LogError($"Didn't found meshrender");
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (_meshRenderer)
            {
                if (Input.GetKeyDown(KeyCode.C))
                {
                    _swapColor = !_swapColor;
                    _meshRenderer.material.color = _swapColor ? color : _baseColor;
                }
            }

            foreach (Transform tr in transform)
            {
                Debug.Log($"Child name : {tr.name}");
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                foreach (Transform tr in transform)
                {
                    tr.localScale *= 0.75f;
                }
            }
        }
    }
}
