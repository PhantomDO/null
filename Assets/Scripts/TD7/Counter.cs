﻿using System;
using System.Collections;
using UnityEngine;

namespace TD7
{
    public class Counter : MonoBehaviour
    {
        private void Start()
        {
            Movement movement = GameObject.FindObjectOfType<Movement>();
            StartCoroutine(LaunchVoitureAfterXSeconds(UnityEngine.Random.Range(1f, 5f), movement));
        }

        IEnumerator LaunchVoitureAfterXSeconds(float seconds, Movement movement)
        {
            if (!movement)
            {
                Debug.LogError($"No movement script found.");
                yield break;
            }

            float i = 0;
            do
            {
                Debug.Log($"Movement launch in {seconds - i} seconds...");
                i += Time.deltaTime;
                yield return null;
            } while (i <= seconds);

            Debug.Log($"GO!");

            if (!movement.isActiveAndEnabled)
            {
                movement.enabled = true;
                this.enabled = false;
                yield break;
            }
        }
    }
}